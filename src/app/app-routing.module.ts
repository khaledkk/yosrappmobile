import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {ExploreComponent} from './components/explore/explore.component';/* components use like this*/
const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    component: HomeComponent
  },

  {
    path: 'explore',
    component: ExploreComponent
  },
  {
    path: 'account',
    loadChildren: () =>
      import('./components/account/account.module').then(
        (m) => m.AccountModule
      ),
  },
  {
    path: 'groups',
    loadChildren: () =>
      import('./components/groups/groups.module').then(
        (m) => m.GroupsModule
      ),
  },

  {
    path: 'auctions',
    loadChildren: () =>
      import('./components/auctions/bids.module').then(
        (m) => m.BidsModule
      ),
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
