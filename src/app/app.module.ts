import { NgModule , CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { RouteReuseStrategy } from '@angular/router';
import {BidsModule} from './components/auctions/bids.module';
import {GroupsModule} from './components/groups/groups.module';
import {SharedModule} from './components/shared/shared.module';
import { HomeComponent } from './components/home/home.component';
import { ExploreComponent } from './components/explore/explore.component';
import { NavComponent } from './components/nav/nav.component';
import { AccountModule } from './components/account/account.module';
import {BrowserModule} from '@angular/platform-browser';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent,HomeComponent ,ExploreComponent,NavComponent],
  entryComponents: [],
  imports: [BrowserModule,IonicModule.forRoot(), AppRoutingModule, NgbModule,BidsModule,SharedModule,GroupsModule,AccountModule],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}
