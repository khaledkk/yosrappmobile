import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { BidsComponent } from './bids/bids.component';
import { BidDetailsComponent } from './bid-details/bid-details.component';
import { CreateBidComponent } from './create-bid/create-bid.component';
import {bidsRoutingModule} from './bids-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    BidsComponent,
    BidDetailsComponent,
    CreateBidComponent,

  ],
  imports: [
    bidsRoutingModule,
    SharedModule


  ],
  exports: [
  ],
  providers: [],  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class BidsModule {
}
