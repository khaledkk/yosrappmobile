
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BidsComponent } from './bids/bids.component';
import { BidDetailsComponent } from './bid-details/bid-details.component';
import { CreateBidComponent } from './create-bid/create-bid.component';

const routes: Routes = [
  { path: '', component: BidsComponent },
  { path: 'bid-details', component: BidDetailsComponent },
  { path: 'create-bid', component: CreateBidComponent}


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class bidsRoutingModule {
}
