import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  constructor() { }
 
  ngOnInit() {}
  slideOpts = {
    initialSlide: 1,
    speed: 400,
    slidesPerView: 1,
   // spaceBetween: 10,
   breakpoints: {
    '@0.75': {
      slidesPerView: 3,
      spaceBetween: 20,
    },
    '@1.00': {
      slidesPerView: 3,
      spaceBetween: 40,
    },
    '@1.50': {
      slidesPerView: 4,
      spaceBetween: 50,
    },
  }
  };

  storyOpts = {
   initialSlide: 2,
    speed: 400,
    slidesPerView: 2.5,
 spaceBetween: 6,
   breakpoints: {
    '@0.75': {
      slidesPerView: 3,
      spaceBetween: 20,
    },
    '@1.00': {
      slidesPerView: 3,
      spaceBetween: 40,
    },
    '@1.50': {
      slidesPerView: 4,
      spaceBetween: 50,
    },
  }
  };
}
