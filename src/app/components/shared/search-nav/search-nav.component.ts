import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-search-nav',
  templateUrl: './search-nav.component.html',
  styleUrls: ['./search-nav.component.scss'],
})
export class SearchNavComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }
  name;
  ngOnInit() {
    this.name= this.route.component['name'];

  }

}
