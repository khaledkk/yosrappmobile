import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

import { AccountComponent } from './account/account.component';
import{accountRoutingModule} from './account-routing.module'
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    AccountComponent
  ],
    imports: [
      accountRoutingModule,
        SharedModule


    ],
  exports: [

  ],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AccountModule {
}
