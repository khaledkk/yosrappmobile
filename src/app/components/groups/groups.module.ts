import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

import { TeamsComponent } from './teams/teams.component';
import{groupsRoutingModule} from './groups-routing.module'
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    TeamsComponent
  ],
    imports: [
        groupsRoutingModule,
        SharedModule


    ],
  exports: [

  ],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class GroupsModule {
}
