import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-explore',
  templateUrl: './explore.component.html',
  styleUrls: ['./explore.component.scss'],
})
export class ExploreComponent implements OnInit {

  constructor() { }

  ngOnInit() {}
  categoryOpts = {
    initialSlide: 1,
    speed: 400,
    slidesPerView: 4.2,
   // spaceBetween: 10,
   breakpoints: {
    '@0.75': {
      slidesPerView: 3,
      spaceBetween: 20,
    },
    '@1.00': {
      slidesPerView: 3,
      spaceBetween: 40,
    },
    '@1.50': {
      slidesPerView: 4,
      spaceBetween: 50,
    },
  }
  };

  cardOpts = {
    initialSlide: 1,
    speed: 400,
    slidesPerView: 2.5,
    spaceBetween: 6,
   breakpoints: {
    '@0.75': {
      slidesPerView: 3,
      spaceBetween: 20,
    },
    '@1.00': {
      slidesPerView: 3,
      spaceBetween: 40,
    },
    '@1.50': {
      slidesPerView: 4,
      spaceBetween: 50,
    },
  }
  };
}
